# Le futuriste's portfolio 2022 version

This is the 2022 version of my portfolio.

Workflow:

All the text content or structured content I want to put on the website is stored in YAML file to beeasy to change and work with. The YAML can be used outside of the website to generate resume for example.

Then I use PHP and [Twig](https://twig.symfony.com/) to render the HTML and [SASS](sass-lang.com) to add a little bit of style. Webpack is used to compile to SASS to CSS and pack the JavaScript.

Unlike the previous version of this website, it doesn't require the use of a database. 

Appart from the language handling, the site doesn't necessarily need to have PHP generate the page each time so it can be build to HTML files by a script and cached. So the whole website can run without PHP.

## Requirements

- PHP >=8.1
- Node >=16.16
- Yarn >=1.22

## Installation

```
git clone blahblah
composer install
yarn
yarn build
```

You may want to run the `build_static.sh` if you want to serve the site without PHP.

## Credits

- [Tabler icons](https://tabler-icons.io)
- [Libre Baskerville](https://github.com/impallari/Libre-Baskerville)
- [Axios](https://axios-http.com/)

## Current structure

- Quick facts about me (Intro)
- Tech wall
- Pro projects
- Side projects
- Find me on (Links)
<!-- - Volunteering -->
- Contact form
<!-- - Reading list -->

## Another structure to try: tabbed structure

The main page: quick facts and the nav bar would be on the left instead of at the top

## Todo

- Add a little bit more style for the project page
- Compress images for the projects slide show
- Add some URL redirection
- Improve accessibility
- Add HTML for the semantic web
- Add link to blog and various links to specifics articles (for example about a particular project or about my computing setup)
- Add PGP email public key
- Extend We Robot
- Extend RetroBox
- Extend French robotic cup
- Optimize for page size
- Optimize images with dithering?

## Technical issues or comments

If you have any comments on this website, please reach to me via [email](mailto:mail@matthieubessat.fr)

