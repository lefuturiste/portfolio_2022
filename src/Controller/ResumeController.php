<?php

namespace App\Controller;

use App\Middlewares\LocalizationMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;
use Twig\Lexer;

class ResumeController extends AbstractController
{
    public function getHTMLResume(ServerRequestInterface $request, ResponseInterface $response)
    {
        $resume = $this->container->get('getResumeData')();
        $locale = $request->getAttribute('locale');
        $getLocalizedStr = $this->container->get('getLocalizedStr');
        $getDynLocalizedStr = $this->container->get('getDynLocalizedStr');

        
        return $this->container->get(Twig::class)                   
                               ->render($response, 'resume.html.twig', [
                                   ...$resume 
                               ]);
    }
    
    public function getLatexResume(ServerRequestInterface $request, ResponseInterface $response)
    {
        $resume = $this->container->get('getResumeData')();
        $locale = $request->getAttribute('locale');
        $getLocalizedStr = $this->container->get('getLocalizedStr');
        $getDynLocalizedStr = $this->container->get('getDynLocalizedStr');

        $twig = $this->container->get(Twig::class)->getEnvironment();  
        $twig->setLexer(new Lexer($twig, [
            'tag_block' => ['(%', '%)'],
            'tag_comment' => ['(!', '!)'],
            'tag_variable' => ['((', '))'],
        ]));
        
        $out = $twig->load('cv.tex')->render([
            ...$resume,
            // Allow to generate annonymized resume/CV (sort of)
            'anno' => in_array(($request->getQueryParams()['anno'] ?? false), ['true', 'yes'])
        ]);

        $response->getBody()->write($out);
        return $response
            ->withHeader('Content-Type', 'application/x-tex')
            ->withStatus(200);
    }

    public function getJsonResume(ServerRequestInterface $request, ResponseInterface $response)
    {
        $resume = $this->container->get('getResumeData')();
        $locale = $request->getAttribute('locale');
        $getLocalizedStr = $this->container->get('getLocalizedStr');
        $getDynLocalizedStr = $this->container->get('getDynLocalizedStr');

        // rendering following the json resume spec
        $jsonResume = [
            'basics' => [
                'name' => $resume['firstName'] . ' ' . $resume['lastName'],
                'label' => $getDynLocalizedStr($locale, $resume['label']),
                'image' => 'https://matthieubessat.fr/imgs/profile-300.jpg',
                'email' => $resume['email'],
                'url' => $resume['website'],
                'summary' => $getDynLocalizedStr($locale, $resume['summary']),
                'location' => [
                    // 'address' => $resume['location']['address'],
                    'postalCode' => $resume['location']['city']['postalCode'],
                    'city' => $resume['location']['city']['name'],
                    'countryCode' => $resume['location']['country']['code'],
                    'region' => $getDynLocalizedStr($locale, $resume['location']['region']['name']),
                ],
                'profiles' => array_map(
                    fn ($l) => [
                        'network' => $l['name'],
                        'username' => $l['username'],
                        ...(isset($l['url']) ? ['url' => $l['url']] : [])
                    ], $resume['links']
                    // array_filter($resume['links'], fn ($l) => !$l['legacy']) 
                )
            ],
            'work' => array_map(
                fn ($pro) => [
                    'name' => $getDynLocalizedStr($locale, $pro['name']),
                    'position' => 'employee',
                    'summary' => $getDynLocalizedStr($locale, $pro['description']),
                    'startDate' => $pro['from'] ?? $pro['date'],
                    'endDate' => $pro['to'] ?? $pro['date'],
                    'url' => $pro['link'],
                    'keywords' => $pro['technologies'],
                    'highlights' => ($pro['highlights'] ?? [])[$locale] ?? [],
                    // 'type' => $pro['deliverableType']
                ],
                $resume['pro_projects']
            ),
            'projects' => array_values(array_map(
                fn ($pro) => [
                    'name' => $getDynLocalizedStr($locale, $pro['name']),
                    'description' => $getDynLocalizedStr($locale, $pro['description']),
                    'startDate' => $pro['from'] ?? $pro['date'],
                    'endDate' => $pro['to'] ?? $pro['date'],
                    'url' => $pro['link'],
                    'keywords' => $pro['technologies'],
                    'type' => $pro['deliverableType'],
                    'highlights' => ($pro['highlights'] ?? [])[$locale] ?? [],
                ],
                $resume['side_projects']
            )),
            'volunteer' => array_map(
                fn ($exp) => [
                    'organization' => $exp['organization'],
                    'summary' => $getDynLocalizedStr($locale, $exp['description']),
                    'position' => 'Volunteer',
                    'startDate' => $exp['from'],
                    'endDate' => $exp['to']
                ],
                $resume['volunteering']
            ),
            'education' => array_map(
                fn ($form) => [
                    'institution' => $form['facility']['name'],
                    'studyType' => $form['name'],
                    'startDate' => $form['from'],
                    'endDate' => $form['to'] 
                ],
                $resume['education']
            ),
            'skills' => array_map(
                fn ($skill) => [
                    'name' => $getDynLocalizedStr($locale, $skill['name']),
                    'level' => 'Master',
                    'keywords' => array_map(fn ($h) => $getDynLocalizedStr($locale, $h), $skill['keywords'] ?? [])
                ],
                $resume['skills']
            ),
            'languages' => array_map(
                fn ($l) => [
                    'language' => $getLocalizedStr($locale, $l['long']),
                    'fluency' => $getLocalizedStr($locale, 'fluency.' . $l['level']) 
                ],
                $resume['languages']
            ),
            'interests' => array_map(
                fn ($i) => [
                    'name' => $getDynLocalizedStr($locale, $i['name']),
                    'keywords' => array_map(fn ($h) => $getDynLocalizedStr($locale, $h), $i['keywords'] ?? [])
                ],
                $resume['interests']
            )
        ];

        return $this->json($response, $jsonResume);
    }
}
