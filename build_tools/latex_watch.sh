#!/bin/sh

# Variation of the latex build script but to compile on file change under templates, config or src
# Expected working directory: Root of the project
# 

./build_tools/latex.sh
[ $? -ne 0 ] && exit 1

while :
do
    inotifywait -e modify --recursive templates/ src/ config/
    echo "file has changed"
    ./build_tools/latex.sh
done


