<?php

use App\App;
use Symfony\Component\Yaml\Yaml;

function getYAML($name) {
    $path = App::getBasePath() . '/' . $name;
    return Yaml::parseFile($path);
}

$misc = getYAML('config/misc.yaml');

return [
    'basePath' => App::getBasePath(),
    'misc' => $misc,
    'last_update' => new DateTime($misc['updated_at']),
    'intro' => getYAML('config/intro.yaml'),
    'links' => getYAML('config/links.yaml')['links'],
    'technologies' => getYAML('config/technologies.yaml')['technologies'],
    'skills' => getYAML('config/skills.yaml')['skills'],
    'education' => getYAML('config/education.yaml')['education'],
    'projects' => getYAML('config/projects.yaml')['projects'],
    'volunteering' => getYAML('config/volunteering.yaml')['volunteering'],
    'locales' => [
        'fr' => new \Adbar\Dot(getYAML('assets/locales/fr.yaml')),
        'en' => new \Adbar\Dot(getYAML('assets/locales/en.yaml'))
    ],
];
