const path = require('path')
const GlobImporter = require('node-sass-glob-importer')
const TerserPlugin = require("terser-webpack-plugin")

const isDev = process.env.NODE_ENV === 'development'

const buildTarget = process.env.BUILD_TARGET ?? 'app'

module.exports = {
  mode: isDev ? 'development' : 'production',
  watch: isDev,

  devtool: isDev ? 'cheap-module-source-map' : 'source-map',

  entry: [
    './assets/scripts/main.js',
    './assets/styles/main.scss'
  ],

  output: {
    path: path.resolve(__dirname, './public/dist/'),
    filename: 'app.min.js',
  },

  module: {
    rules: [{
      test: /\.(scss)/,
      use: [
        {
          loader: 'file-loader',
          options: {
            outputPath: './',
            name: buildTarget + '.min.css'
          }
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            additionalData: "$buildTarget: " + (buildTarget) + ';',
            sassOptions: {
              importer: GlobImporter(),
              outputStyle: "compressed"
            }
          }
        }
      ]
    }]
  },
  plugins: [],
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin()
    ]
  }
}
