<?php

use App\App;
use App\Middlewares\TwigLocalizationMiddleware;
use App\Util\ContainerBuilder;
use App\Utils\DotEnv;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use DI\Container;

use App\Middlewares\LocalizationMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

use function App\addRoutes;

require '../vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

date_default_timezone_set('Europe/Paris');

App::setBasePath(dirname(__DIR__));

$container = ContainerBuilder::direct();
$app = AppFactory::createFromContainer($container);

$app->addRoutingMiddleware();
$errorMiddleware = $app->addErrorMiddleware(true, true, true);

// we place the definition BEFORE the localization middleware to get the locale attribute
$app->add(new TwigLocalizationMiddleware($container));
$app->add($container->get(LocalizationMiddleware::class));

$app->add(TwigMiddleware::create($app, $container->get(Twig::class)));

require '../src/routes.php';

addRoutes($app);

$app->run();
