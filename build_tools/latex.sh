#!/bin/sh

# this script will build the latex resume in a particular language
# working directory: root of the project

echo "Latex script"

if [ -z $SERVER_URL ]
then
    echo "Error: you need to provide SERVER_URL to latex build script" >&2
    exit 1
fi

[ ! -d build/latex ] && mkdir -p build/latex

if [ -z $TARGET_LOCALE ]
then
    echo "Warning: no target locale found will assume fr"
    TARGET_LOCALE=fr
fi

SOURCE=build/latex/resume_$TARGET_LOCALE

echo "Source=$SOURCE"

if [ -n $OPTS ]
then
    EXTRA_URL="?$OPTS"
fi

wget $SERVER_URL/latex-resume$EXTRA_URL --header="Accept-Language: $TARGET_LOCALE" -O $SOURCE.tex
pdflatex --output-directory=build/latex -halt-on-error -shell-escape $SOURCE.tex

[ ! -d ./.latex_cache ] && mkdir ./.latex_cache

cp $SOURCE.pdf .latex_cache/

echo "Compilation attempt for locale $TARGET_LOCALE terminated"

