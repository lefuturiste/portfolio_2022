<?php

namespace App\Controller;

use Psr\Http\Message\ResponseInterface;

class PingController extends AbstractController
{
    public function getPing($_, ResponseInterface $response)
    {
        return $this->json($response, "Pong!");
    }
}
