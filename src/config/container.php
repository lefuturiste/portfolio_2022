<?php

use Psr\Container\ContainerInterface;
use Slim\Views\Twig;
use App\Middlewares\LocalizationMiddleware;
use Twig\Extra\Intl\IntlExtension;

function formatDate($raw) {
    if (strlen($raw) <= 4) {
        $w = \DateTime::createFromFormat('Y', $raw);
        return $w;
    }
    return new \DateTime($raw);
}

return [
    'getLocalizedStr' => function(ContainerInterface $container) {
        return function($locale, $inp) use ($container) {
            if (!$container->get('locales')[$locale]->has($inp)) {
              return $inp;
            }

            return $container->get('locales')[$locale]->get($inp);
        };
    },
    'getDynLocalizedStr' => function(ContainerInterface $container) {
        return function($locale, $inp) {
            if (is_array($inp)) {
                return $inp[$locale];
            }
            if (is_string($inp)) {
                return $inp;
            }
            
            return 'invalid';
        };
    },
    Twig::class => function(ContainerInterface $container) {
        $twig = Twig::create('../templates', ['cache' => false]);
        $twig->getLoader()->addPath('../public/imgs', 'imgs');
        $twig->addExtension(new IntlExtension());

        $twig->getEnvironment()->addFilter(new \Twig\TwigFilter('formatDate', function (\Twig\Environment $env, $inp) use ($container) {
            $locale = $env->getGlobals()['locale'];
            $formatter = new \IntlDateFormatter($locale);
            if ($inp instanceof \DateTime) {
                $d = $inp;
            } else {
                $d = (new \DateTime($inp));
                if (strlen($inp) <= 4) {
                    return $inp;
                }
            }
            $formatter->setPattern('MMMM YYYY');

            return $formatter->format($d);
        }, ['needs_environment' => true]));

        $twig->getEnvironment()->addFunction(new \Twig\TwigFunction('getDynLocalizedStr', function (\Twig\Environment $env, $inp) use ($container) {
            $locale = $env->getGlobals()['locale'];

            return $container->get('getDynLocalizedStr')($locale, $inp);
        }, ['needs_environment' => true]));

        $twig->getEnvironment()->addFunction(new \Twig\TwigFunction('getLocalizedStr', function (\Twig\Environment $env, $inp) use ($container) {
            $locale = $env->getGlobals()['locale'];

            
            return $container->get('getLocalizedStr')($locale, $inp);
        }, ['needs_environment' => true]));

        // $twig->getEnvironment()->addFunction(new \Twig\TwigFunction('resolveImg', function ($inp) {
        //     if (strpos($inp, '$commons') !== false) {
        //         return "commons/" . str_replace($inp, '$commons', ''); 
        //     }
        //     if (strpos($inp, '$project') !== false) {
        //         return "commons/" . str_replace($inp, '$commons', ''); 
        //     }
        // });
        $twig->getEnvironment()->addFunction(new \Twig\TwigFunction('getLocale', function (\Twig\Environment $env) {
            return $env->getGlobals()['locale'];
        }, ['needs_environment' => true]));

        $twig->getEnvironment()->addGlobal('lastUpdate', $container->get('last_update'));

        $twig->getEnvironment()->addFilter(new \Twig\TwigFilter('formatMd', function ($inp) {
            $Parsedown = new Parsedown();

            return $Parsedown->text($inp);
        }));
    

        return $twig;
    },

    LocalizationMiddleware::class => function () {
        $availableLocales = ['en', 'fr'];
        $defaultLocale = 'en';
        return new LocalizationMiddleware($availableLocales, $defaultLocale);
    },

    'getResumeData' => function (ContainerInterface $container) {
        return function ($priority = 2) use ($container) {
            $allTechnologies = $container->get('technologies');
            $technologies = array_filter($allTechnologies, fn ($t) => !isset($t['hidden']) || !$t['hidden']);
            $intro = $container->get('intro');
            $age = (new \DateTime('now'))->diff(new \DateTime($intro['bornDate']))->y;

            $processExp = function ($exp) use ($allTechnologies) {
                if (isset($exp['thumbnail'])) {
                    $src = $exp['thumbnail']['src'];
                    if (strpos($src, '$project') !== false) {
                        $exp['thumbnail']['src'] = str_replace('$project', 'projects/' . $exp['id'] . '/', $src);
                    }
                }
                if (isset($exp['link'])) {
                    $platform = 'web';
                    if (strpos($exp['link'], 'github.com') !== false) { $platform = 'github'; } 
                    if (strpos($exp['link'], 'gitlab.com') !== false) { $platform = 'gitlab'; } 
                    $exp['link_platform'] = $platform;
                    $exp['link_icon'] = match ($platform) {
                        'web' => 'icons/link.svg',
                        'github' => 'logos/github.svg',
                        'gitlab' => 'logos/gitlab.svg'
                    };
                    $exp['link_alt'] = str_replace('https://', '', $exp['link']);
                }
                if (isset($exp['technologies'])) {
                    $exp['raw_technologies'] = $exp['technologies'];
                    $exp['technologies'] = array_map(function ($tid) use ($allTechnologies) {
                        $founds = array_values(array_filter($allTechnologies, fn ($t) => $t['id'] === $tid));
                        if (count($founds) > 0) {
                            return $founds[0];
                        }
                        return ['id' => $tid, 'name' => ucfirst($tid)];
                    }, $exp['technologies']);
                }
                if (isset($exp['from'])) {
                    if (strlen($exp['from']) <= 4) {
                        $exp['from_year'] = true;    
                    }
                    $exp['from'] = formatDate($exp['from']);
                }
                if (isset($exp['to'])) {
                    if (strlen($exp['to']) <= 4) {
                        $exp['to_year'] = true;    
                    }
                    $exp['to'] = formatDate($exp['to']);
                }
                if (isset($exp['date'])) {
                    if (strlen($exp['date']) <= 4) {
                        $exp['date_year'] = true;    
                    }
                    $exp['date'] = formatDate($exp['date']);
                }
                if (!isset($exp['date'])) {
                    $exp['date'] = $exp['from'];
                }

                return $exp;
            };

            $projects = array_map($processExp, $container->get('projects'));

            // sort projects by date
            usort($projects, fn ($b, $a) => $a['date']->getTimestamp() - $b['date']->getTimestamp());

            $volunteering = array_map($processExp, $container->get('volunteering'));
            usort($volunteering, fn ($b, $a) => $a['date']->getTimestamp() - $b['date']->getTimestamp());
            $skills = $container->get('skills');
            $skills = array_map(function ($s) use ($allTechnologies) {
                    $s['keywords'] = array_map(function ($tid) use ($allTechnologies) {
                        $founds = array_values(array_filter($allTechnologies, fn ($t) => $t['id'] === $tid));
                        if (count($founds) > 0) {
                            return $founds[0]['name'];
                        }
                        return ucfirst($tid);
                    }, $s['keywords']);
                    return $s;
            }, $skills);

            $education = $container->get('education');
            $links = array_map(function ($link) {
                if (!isset($link['thumbnail'])) {
                    $link['thumbnail'] = ['src' => $link['id'] . '.svg', 'alt' => $link['id'] . " logo"];
                }

                return $link;
            }, $container->get('links'));
            $linksByIds = array_combine(array_map(fn ($l) => $l['id'], $links), $links);

            return [
                'misc' => $container->get('misc'),
                'links' => $links,
                'links_by_ids' => $linksByIds,
                'technologies' => $technologies,
                'pro_projects' => array_filter($projects, fn ($pro) => in_array($pro['type'], ['professional']) && $pro['priority'] >= $priority),
                'side_projects' => array_filter($projects, fn ($pro) => in_array($pro['type'], ['associative', 'entrepreneurship', 'opensource', 'learning', 'side']) && $pro['priority'] >= $priority),
                'volunteering' => $volunteering,
                'education' => $education,
                'skills' => $skills,
                ...$container->get('misc'),
                ...$container->get('intro'),
                'age' => $age
            ];
        };
    } 
];
