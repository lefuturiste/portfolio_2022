<?php

namespace App\Middlewares;


use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Http\Factory\DecoratedResponseFactory;
use Slim\Http\Response;
use Slim\Psr7\Factory\ResponseFactory;
use Slim\Psr7\Factory\StreamFactory;
use Psr\Container\ContainerInterface;
use Slim\Views\Twig;

class TwigLocalizationMiddleware {

    public function __construct(
        private ContainerInterface $container
    )
    {

    }


    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $env = $this->container->get(Twig::class)->getEnvironment();
        $env->addGlobal('locale', $request->getAttribute('locale'));
        $env->addGlobal('enableLocaleSwitch', $request->hasHeader('Enable-Locale-Switch') ? ($request->getHeader('Enable-Locale-Switch')[0] === 'yes') : true);


        return $handler->handle($request);
    }
}
