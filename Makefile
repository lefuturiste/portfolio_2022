.SILENT:
.PHONY: build server serve help test dev resume
PORT?=8001
HOST?=127.0.0.1

build:
	./build_tools/static_build.sh
resume:
	./build_tools/server.sh start
	env TARGET_LOCALE=fr SERVER_URL=127.0.0.1:8442 ./build_tools/latex.sh
	env TARGET_LOCALE=en SERVER_URL=127.0.0.1:8442 ./build_tools/latex.sh
	./build_tools/server.sh stop
serve: server
dev: server
server:
	php -S $(HOST):$(PORT) -t public
test:
	php ./vendor/bin/phpunit --stop-on-failure tests

