<?php

namespace App\Controller;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

abstract class AbstractController
{

    public function __construct(protected ContainerInterface $container)
    {
    }

    public function redirect(ResponseInterface $response, string $location)
    {
        return $response->withStatus(302)->withHeader('Location', $location);
    }

    public function json(
        ResponseInterface $response,
        $data = ['success' => true],
        int $status = 200
    ): ResponseInterface
    {
        $response->getBody()->write(json_encode($data));
        return $response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($status);
    }

}
