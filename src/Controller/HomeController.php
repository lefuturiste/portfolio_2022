<?php

namespace App\Controller;

use App\Middlewares\LocalizationMiddleware;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Views\Twig;

class HomeController extends AbstractController
{
    public function getHome(ServerRequestInterface $request, ResponseInterface $response)
    {
        // $locale = $request->getAttributes()['locale'];
        // dd($locale);
        $technologies = array_filter($this->container->get('technologies'), fn ($t) => !isset($t['hidden']) || !$t['hidden']);
        $intro = $this->container->get('intro');
        $age = (new \DateTime('now'))->diff(new \DateTime($intro['bornDate']))->y;


        $processExp = function ($exp) {
            if (isset($exp['thumbnail'])) {
                $src = $exp['thumbnail']['src'];
                if (strpos($src, '$project') !== false) {
                    $exp['thumbnail']['src'] = str_replace('$project', 'projects/' . $exp['id'] . '/', $src);
                }
            }

            return $exp;
        };

        $projects = array_map($processExp, $this->container->get('projects'));
        $volunteering = array_map($processExp, $this->container->get('volunteering'));

        return $this->container->get(Twig::class)->render(
            $response, 'home/home.html.twig',
            [
                'links' => array_map(function ($link) {
                    if (!isset($link['thumbnail'])) {
                        $link['thumbnail'] = ['src' => $link['id'] . '.svg', 'alt' => $link['id'] . " logo"];
                    }

                    return $link;
                }, $this->container->get('links')),
                'technologies' => $technologies,
                'pro_projects' => array_filter($projects, fn ($pro) => in_array($pro['type'], ['professional']) && $pro['priority'] >= 2),
                'side_projects' => array_filter($projects, fn ($pro) => in_array($pro['type'], ['associative', 'entrepreneurship', 'opensource', 'learning', 'side']) && $pro['priority'] >= 2),
                'volunteering' => $volunteering,
                ...$this->container->get('misc'),
                ...$this->container->get('intro'),
                'age' => $age
            ]
        );
    }

    public function getAbout(ServerRequestInterface $request, ResponseInterface $response)
    {
        return $this->container->get(Twig::class)->render(
            $response, 'about.html.twig',
            [
                'email' => $this->container->get('intro')['email']
            ]
        );
    }

    public function getProject(ServerRequestInterface $request, ResponseInterface $response)
    {
        $id = $request->getAttribute('id');
        $projects = [...$this->container->get('projects')];
        $projectsCandidates = array_values(array_filter($projects, fn ($p) => $p['id'] == $id));
        if (count($projectsCandidates) === 0) {
            // TODO: add proper response body
            $response->getBody()->write("Project not found");
            return $response
                ->withStatus(404);
        }
        $project = $projectsCandidates[0];

        $technologies = $this->container->get('technologies');

        $projectTechnologies = array_map(fn ($tId) => array_values(array_filter($technologies, fn ($t) => $t['id'] == $tId))[0], $project['technologies']);

        return $this->container->get(Twig::class)->render(
            $response, 'project.html.twig',
            [
                'project' => [...$project, 'technologies' => $projectTechnologies]
            ]
        );
    }
    
    public function switchToLocale(ServerRequestInterface $request, ResponseInterface $response)
    {
        $newLocale = substr($request->getUri()->getPath(), 1);
        $redir = $request->hasHeader('Referer') ? $request->getHeader('Referer') : '/';
        return $this->container->get(LocalizationMiddleware::class)->withSetCookieLocale(
            $response
                ->withStatus(302)
                ->withAddedHeader('Location', $redir),
            $newLocale
        );
    }
}
