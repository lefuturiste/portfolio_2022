#!/bin/sh

# This script will start a php server in a GNU Screen and save the id in the build directory to clean it
# This script must have a working directory of ./
# Two sub commands for this script: start and stop

echo "run script inside $(pwd)"

op='start'

if [ $# -ge 1 ]
then
    op=$1
fi

DEFAULT_PORT=8442

if [ -z $PORT ]
then
    echo "No port found assuming $DEFAULT_PORT"
    PORT=$DEFAULT_PORT
fi

startCmd() {
    existingPID=`lsof -i :$PORT | awk 'NR!=1{print $2}'`

    if [ ${#existingPID} -gt 2 ]
    then
        echo "killing existing server $existingPID"
        kill $existingPID
    fi

    serverUrl=127.0.0.1:$PORT
    echo "Trying to start php on $serverUrl"
    php -S $serverUrl -t ./public &

    phpPID=$!

    echo $phpPID > ./build/tmp_build_server_pid

    echo "PHP server running with PID $phpPID"
    sleep 0.2
}

stopCmd() {
    kill `cat ./build/tmp_build_server_pid` 
}

if [ $op = 'start' ]
then
    startCmd
elif [ $op = 'stop' ]
then
    stopCmd
else
    echo "Invalid command, usage: server start|stop"
    exit 1
fi

