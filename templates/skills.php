<?php
$technologies = [
    [
        'name' => 'HTML 5',
        'image' => 'html5.svg'
    ],

    [
        'name' => 'CSS 3',
        'image' => 'css3.svg'
    ],

    [
        'name' => 'WebPack',
        'image' => 'webpack.svg'
    ],

    [
        'name' => 'WebPack',
        'image' => 'webpack.svg'
    ]
];
?>

<section class="skills">
    <div class="container">
        <div class="section-title">
            <h2>Skills and technologies I work with</h2>
            <span></span>
        </div>
        <div class="skills-mosaic">
            <?php
            foreach ($technologies as $tech) {
            ?>
            <img src="imgs/technos/<?= $tech['image'] ?>" alt="<?= $tech['name'] ?>'s logo" />
            <?php
            }
            ?>
        </div>
    </div>
</section>