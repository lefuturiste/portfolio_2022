<?php

namespace App;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Routing\RouteCollectorProxy;

function addRoutes(\Slim\App $app): void
{
    $container = $app->getContainer();

    $app->get('/', [Controller\HomeController::class, 'getHome']);
    $app->get('/projects/{id}', [Controller\HomeController::class, 'getProject']);
    
    $app->get('/html-resume', [Controller\ResumeController::class, 'getHTMLResume']);
    $app->get('/json-resume', [Controller\ResumeController::class, 'getJsonResume']);
    $app->get('/latex-resume', [Controller\ResumeController::class, 'getLatexResume']);
    // $app->get('/resume.json', [Controller\ResumeController::class, 'getResume']);
    // $app->get('/resume.xml', [Controller\ResumeController::class, 'getResume']);
    // $app->get('/resume.pdf', [Controller\ResumeController::class, 'getResume']);

    $app->get('/about', [Controller\HomeController::class, 'getAbout']);
    $app->get('/ping', [Controller\PingController::class, 'getPing']);

    $addDoNotSetLocaleCookieMiddleware = function (ServerRequestInterface $req, RequestHandlerInterface $handler) {
        return $handler->handle($req->withAttribute('doSetLocaleCookie', false));
    };

    $app->get('/en', [Controller\HomeController::class, 'switchToLocale'])->add($addDoNotSetLocaleCookieMiddleware);
    $app->get('/fr', [Controller\HomeController::class, 'switchToLocale'])->add($addDoNotSetLocaleCookieMiddleware);
}
